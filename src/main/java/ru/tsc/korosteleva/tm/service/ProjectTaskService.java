package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectTaskRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;

public class ProjectTaskService implements IProjectTaskService {

    final IProjectTaskRepository projectTaskRepository;

    public ProjectTaskService(final IProjectTaskRepository projectTaskRepository) {
        this.projectTaskRepository = projectTaskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        projectTaskRepository.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        projectTaskRepository.unbindTaskFromProject(projectId, taskId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        projectTaskRepository.removeProjectById(projectId);
    }
}
