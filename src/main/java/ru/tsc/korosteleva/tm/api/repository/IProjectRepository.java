package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    boolean existById(String id);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    void clear();

}