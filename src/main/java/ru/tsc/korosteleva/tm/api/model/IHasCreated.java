package ru.tsc.korosteleva.tm.api.model;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setDateCreated(Date created);

}
