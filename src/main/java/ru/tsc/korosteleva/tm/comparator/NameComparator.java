package ru.tsc.korosteleva.tm.comparator;

import ru.tsc.korosteleva.tm.api.model.IHasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<IHasName> {

    INSTANCE;

    @Override
    public int compare(final IHasName name1, final IHasName name2) {
        if (name1 == null || name2 == null) return 0;
        if (name1.getName() == null || name2.getName() == null) return 0;
        return name1.getName().compareTo(name2.getName());
    }
}
